from game.game import GameServer
from game.get_txt import get_txt


def main():
    GameServer(
        instructions=get_txt('txt/instructions.txt')
    ).listen()


if __name__ == "__main__":
    main()
