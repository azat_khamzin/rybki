from game.get_txt import get_txt
import os
from client import Client
from random import randint

fish_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'txt\\fish.txt')
trash_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'txt\\trash.txt')
fish = get_txt(fish_path)
trash = get_txt(trash_path)


def game_choice(Client, data):
    if data == fish:
        simple_send_data(Client, "1")


def get_ready(Client):
    Client.send_data("changeStatus")


def change_name(Client):
    Client.send_data(f"nick:{randint(1, 100000000000)}")


def simple_send_data(Client, data):
    Client.sock.send(data.encode('utf-8'))
