import socket
import threading
from game.get_txt import get_txt
import os
from random import randint

fish_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'game-main/txt/fish.txt')
trash_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'game-main/txt/trash.txt')
fish = get_txt(fish_path)
trash = get_txt(trash_path)


class Client:
    def __init__(self, host, port=5000, ai=False):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.is_connected = True
        self.ai = ai
        self.fish_viewed = False

    def launch_client(self):
        with self.sock:
            self.sock.connect((self.host, self.port))
            listen_thread = threading.Thread(target=self.listen)
            listen_thread.start()
            self.send_data()
            self.is_connected = False
            listen_thread.join()

    def listen(self):
        while self.is_connected:
            data = self.sock.recv(4096).decode('utf-8')

            if "\n\n" in data:
                data_check = data.split("\n\n", 1)[1]
                if data_check == fish:
                    self.fish_viewed = True
            
            print(data)

    def send_data(self):
        if self.ai:
            self.get_ready()
            while True:
                if self.fish_viewed:
                    print("Fish!")
                    self.sock.send("1".encode('utf-8'))
                    self.fish_viewed = False
            
        while True:
            request = input()
            if request == '':
                request = '!'
            self.sock.send(request.encode('utf-8'))
            if request == 'disconnect':
                return

    def get_ready(self):
        self.simple_send_data("changeStatus")

    def simple_send_data(self, data):
        self.sock.send(data.encode('utf-8'))


if __name__ == "__main__":
    HOST = 'localhost'
    client = Client(HOST, ai=True)
    client.launch_client()
